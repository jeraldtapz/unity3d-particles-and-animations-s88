# README #


### Synergy88 Unity3D Particles and Animations Exam ###

### How do I get set up? ###

* Open Main.unity scene file 

## Notes ##
* Not all animations and particle systems were successfully recreated
* [Windows .exe build](https://bitbucket.org/jeraldtapz/unity3d-particles-and-animations-s88/raw/92a34089066917781cd7d7e2740b8762859f040f/Deliverables/Windows/ParticleSystemsAndAnimations.rar).
* [Android .apk build](https://bitbucket.org/jeraldtapz/unity3d-particles-and-animations-s88/raw/92a34089066917781cd7d7e2740b8762859f040f/Deliverables/Android/ParticlesandAnimations.apk) Smaller file size but no support for x86 devices.
* [Android .apk build with x86 support](https://bitbucket.org/jeraldtapz/unity3d-particles-and-animations-s88/raw/92a34089066917781cd7d7e2740b8762859f040f/Deliverables/Android/ParticlesandAnimations_with_x86support.apk) Bigger file size but supports both ARM and x86 devices.
* All assets used are downloaded from the web.