﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour 
{
    #region Static Fields
    private static UIManager instance;
    public static UIManager Instance
    {
        get
        {
            return instance;
        }

        private set
        {
            instance = value;
        }
    }

    #endregion

    #region Property Fields

    [SerializeField]
    private GameObject[] AnimationPanels;

    [SerializeField]
    private GameObject[] FloatingIconsLeft;     //These are the objects(UI/Icons) that are going to be shown floating in the UI
                                                //We will set this value in the inspector for the moment
                                                //In a real game project, which objects gets to be shown in the UI will depend on the game logic

    [SerializeField]
    private GameObject[] FloatingIconsRight;

    private int CurrentFloatingIconIndexLeft;
    private int CurrentFloatingIconIndexRight;

    private int CurrentIndex;



    #endregion

    #region Unity Life Cycle

    private void Awake()
    {
        Instance = this;
    }
    #endregion 

    #region Public Functionality

    public void OnClickNext()
    {
        AnimationPanels[CurrentIndex].SetActive(false);
        CurrentIndex = (CurrentIndex == AnimationPanels.Length - 1) ? 0 : CurrentIndex + 1;
        AnimationPanels[CurrentIndex].SetActive(true);


        ///Some Hard coded stufffor demonstration purposes
        CurrentFloatingIconIndexLeft = 0;
        CurrentFloatingIconIndexRight = 0;
        if(CurrentIndex == 3)
        {
            FloatingIconsLeft[0].SetActive(true);
            FloatingIconsRight[0].SetActive(true);
        }
    }

    public void ActivateNextFloatingIcon(bool isLeft)
    {
        if(isLeft)
        {
            if (CurrentFloatingIconIndexLeft < FloatingIconsLeft.Length - 1)
            {
                FloatingIconsLeft[++CurrentFloatingIconIndexLeft].SetActive(true);
                FloatingIconsLeft[CurrentFloatingIconIndexLeft].GetComponent<Animator>().enabled = true;
            }
        }
        else
        {
            if(CurrentFloatingIconIndexRight < FloatingIconsRight.Length - 1)
            {
                FloatingIconsRight[++CurrentFloatingIconIndexRight].SetActive(true);
                FloatingIconsRight[CurrentFloatingIconIndexRight].GetComponent<Animator>().enabled = true;
            }
        }
        
    }


    #endregion
}