﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingIconHelper : MonoBehaviour 
{
    //0 for left, 1 for right
	public void ActivateNext(int isLeft)
    {
        UIManager.Instance.ActivateNextFloatingIcon(isLeft == 0);
    }

    public void DisableGameObject()
    {
        gameObject.SetActive(false);
    }
}
