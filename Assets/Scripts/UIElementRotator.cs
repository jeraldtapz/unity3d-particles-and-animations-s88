﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIElementRotator : MonoBehaviour 
{
    #region Property Fields

    [SerializeField]
    private float rotationSpeed;
    #endregion

    #region Unity Life Cycle
    #endregion
	
	private void Update () 
	{
        transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
	}
}
